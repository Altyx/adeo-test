import { createStore } from 'vuex';
import orders from '@/assets/orders.json';
import stocks from '@/assets/magasins.json';
import drones from '@/assets/drones.json';
import clients from '@/assets/clients.json';

// eslint-disable-next-line no-shadow
const getFormattedStocks = (stocks) => {
  const prod = stocks;
  const t = {};
  prod.forEach(({
    id, x, y, stock,
  }) => {
    stock.forEach((p) => {
      t[p.productId] = {
        ...t[p.productId],
        [id]: { x, y, stock: p.quantity },
      };
    });
  });
  return t;
};

export default createStore({
  state: {
    drones,
    stocks: getFormattedStocks(stocks),
    orders,
    plans: [],
    clients,
    ordersPassed: [],
  },
  getters: {
    getAvailableDrones(state) { return state.drones.filter((drone) => drone.autonomy > 0); },
    // eslint-disable-next-line max-len
    getPassedOrder: (state) => (payload) => state.ordersPassed.filter((order) => order.orderId === payload.id
        && order.product === payload.product).length,
    getClient: (state) => (clientId) => state.clients.find((client) => client.id === clientId),
  },
  mutations: {
    updateStock(state, payload) {
      let tmp = state.stocks[payload.productId];
      const { stock } = tmp[payload.magasin];
      tmp = {
        ...tmp,
        [payload.magasin]: {
          ...tmp[payload.magasin],
          stock: stock - 1,
        },
      };
      this.state.stocks[payload.productId] = tmp;
    },
    createPlan(state, payload) {
      state.plans.push({
        drones: payload.drones,
        stores: payload.stores,
        products: payload.products,
        customers: payload.customers,
      });
    },
    updateDrone(state, payload) {
      const tmp = state.drones.findIndex((drone) => drone.id === payload.droneId);
      state.drones[tmp] = {
        ...state.drones[tmp], autonomy: state.drones[tmp].autonomy - payload.distance,
      };
    },
    createOrder(state, payload) {
      state.ordersPassed.push({
        orderId: payload.orderId,
        customer: payload.customer,
        product: payload.product,
      });
    },
  },
  actions: {
  },
  modules: {
  },
});
